// app/routes/protected.js
import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

const { inject: { service } } = Ember;

export default Ember.Route.extend(AuthenticatedRouteMixin,{
    mastodonApi: service('mastodonApi'),
    model: function() {
      console.log('protected route');
      console.log(this.get('mastodonApi').timelines_home());
      return this.get('store').findAll('status');
    }
});
