// app/routes/application.js
import Ember from 'ember';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';

const { inject: { service } } = Ember;

export default Ember.Route.extend(ApplicationRouteMixin,{
  mastodonApi: service(),
  session: service(),
   beforeModel() {
       return this._loadMastodonApi();
     },

   sessionAuthenticated() {
     console.log('sessionAuthenticated');
     this._super(...arguments);
     this._loadMastodonApi();
     //console.log();
   },

     _loadMastodonApi() {
       console.log('_loadmastodonApi');
       let store = this.get('store');
       store.findAll('status').then(function(list){
         list.forEach(function(item) {
           item.deleteRecord();
           item.save();
           //console.log('unload');
         });
       });
       store.findAll('app').then(function(list){
         list.forEach(function(item) {
           item.deleteRecord();
           item.save();
           console.log('unload');
         });
       });
       //this.get('mastodonApi').verify_credentials();
       this.get('mastodonApi').timelines_home();
     }
});
