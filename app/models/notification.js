import DS from 'ember-data';

export default DS.Model.extend({
  type: DS.attr(),
  account: DS.attr(),
  created_at	: DS.attr(),
  status: DS.attr()
});
