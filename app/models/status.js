import DS from 'ember-data';

export default DS.Model.extend({
uri: DS.attr(),
url: DS.attr(),
account: DS.attr(),
in_reply_to_id	: DS.attr(),
in_reply_to_account_id	: DS.attr(),
reblog	: DS.attr(),
content: DS.attr(),
created_at	: DS.attr(),
reblogs_count: DS.attr(),
favourites_count	: DS.attr(),
reblogged	: DS.attr(),
favourited: DS.attr(),
sensitive: DS.attr(),
spoiler_text: DS.attr(),
visibility	: DS.attr(),
media_attachments	: DS.attr(),
mentions	: DS.attr(),
tags	: DS.attr(),
application: DS.attr()
});
