import DS from 'ember-data';

export default DS.Model.extend({
  username: DS.attr(),
acct: DS.attr(),
display_name: DS.attr(),
locked: DS.attr(),
created_at: DS.attr(),
followers_count: DS.attr(),
following_count	: DS.attr(),
statuses_count	: DS.attr(),
note: DS.attr(),
url	: DS.attr(),
avatar: DS.attr(),
avatar_static: DS.attr(),
header: DS.attr(),
header_static: DS.attr()
});
