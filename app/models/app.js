import DS from 'ember-data';

export default DS.Model.extend({
     instance:DS.attr(),
     redirectUri: DS.attr(),
     clientId: DS.attr(),
   clientSecret:  DS.attr()
});
