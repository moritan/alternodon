import Ember from 'ember';
//import Base from 'ember-simple-auth/authenticators/base';
import OAuth2PasswordGrant from 'ember-simple-auth/authenticators/oauth2-password-grant';
const { inject: { service } } = Ember;
export default OAuth2PasswordGrant.extend({
    store: service('store'),
    mastodonApi: service('mastodonApi'),
    tokenEndpoint: '/oauth/token',
    authenticate: function(options) {
        let store   = this.get('store');
        let instance = options.instance;

        store.unloadAll('status');
        if(!instance.startsWith("https://")){
            instance = "https://" + instance;
        }
        this.get('mastodonApi').set('instance',instance);

        return new Ember.RSVP.Promise((resolve, reject) => {
                  let app={};
                  store.findAll('app').then(function(result) {
                  //  if(result.isAny('instance', instance) ){
                      if(false ){
                        console.log('instance exist in base');
                            let allApps =result.filterBy('instance', instance).uniqBy('instance');
                            allApps.forEach(function(item, index, enumerable) {
                              app =item;
                            });
                          }else{
                            console.log('search call api');
                            Ember.$.post( instance +"/api/v1/apps",
                              { client_name: "alternativeClient", redirect_uris: "urn:ietf:wg:oauth:2.0:oob", scopes: "read write follow", scope: "read write follow"}
                            ).done(function( data ) {
                                  let _app={};
                                  _app['instance']=instance;
                                  _app['redirectUri']=data.redirect_uri;
                                  _app['clientId']=data.client_id;
                                  _app['clientSecret']=data.client_secret;
                                  console.log(_app);
                                  app = store.createRecord('app',_app);
                                  app.save();
                                  console.log('app save');


console.log(app);
                          Ember.$.ajax({
                                url: app.get('instance') + '/oauth/token',
                                type: 'POST',
                                data: JSON.stringify({
                                    client_id: app.get('clientId'),
                                    client_secret:app.get('clientSecret'),
                                    scopes: "read write follow",
                                    scope: "read write follow",
                                    grant_type:'password',
                                    username: options.identification,
                                    password: options.password
                                }),
                                contentType: 'application/json;charset=utf-8',
                                dataType: 'json'
                            }).then(function(response) {
                                //  this.get('mastodonApi').verify_credentials();
                                response.instance = instance;
                                Ember.run(function() {
                                    resolve(response);
                                });
                            }, function(xhr) {
                              console.log('error');
                                Ember.run(function() {
                                    reject(response);
                                });
                           });
                          });
                            } 
                }, function() {
                    reject();
                });
              });
    },

    invalidate: function() {
        console.log('invalidate...');
        return    this._super(...arguments);
    }
});
