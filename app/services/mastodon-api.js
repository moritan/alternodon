import Ember from 'ember';

const { inject: { service }, isEmpty } = Ember;

export default Ember.Service.extend({
  session: service('session'),
  store: service('store'),

  verify_credentials(){
    let instance = this.get('instance');
    let store   = this.get('store');
    let self = this;

    if(instance === undefined){
      instance= self.get('session.data.authenticated.instance');
      this.set('instance',instance);
    }
    if(instance === undefined){
      this.get('session').invalidate();
    }

    //console.log('verify_credentials on '+instance);
    this.get('session').authorize('authorizer:mastodon', (headerName, headerValue) => {
            let res= Ember.$.ajax({
              url: instance + '/api/v1/accounts/verify_credentials',
              beforeSend: function(xhr) {
                xhr.setRequestHeader(headerName, headerValue);
              },
              method: 'GET',
              contentType: 'application/json; charset=utf-8',
              dataType: 'json',
              data: JSON.stringify({
                // stuff
              })
            });
             res.then(function(account) {
                var user = store.createRecord('account',account);
                user.save();
                self.set('user',user);
              }, function(reason) {
                // some error handling
                console.log(error);
              });
    });
  },
  timelines_home(){
    let instance = this.get('instance');
    let store   = this.get('store');
    let self = this;

    if(instance === undefined){
      instance= self.get('session.data.authenticated.instance');
      this.set('instance',instance);
    }
    if(instance === undefined){
      if(this.get('session').get('isAuthenticated')){
        this.get('session').invalidate();
      }
      return;
    }

    //console.log('timelines_home on '+instance);
    this.get('session').authorize('authorizer:mastodon', (headerName, headerValue) => {
            let res= Ember.$.ajax({
              url: instance + '/api/v1/timelines/home',
              beforeSend: function(xhr) {
                xhr.setRequestHeader(headerName, headerValue);
              },
              method: 'GET',
              contentType: 'application/json; charset=utf-8',
              dataType: 'json',
              data: JSON.stringify({
                // stuff
              })
            });
             res.then(function(resp) {
               console.log('timeline');
               resp.forEach(function(item, index, enumerable) {
                 var status = store.createRecord('status',item);
                 status.save();
                 //console.log(status);
               });

              }, function(reason) {
                // some error handling
                console.log(error);
              });
    });
  },
  post_status(post_value){
    let instance = this.get('instance');
    let store   = this.get('store');
    let self = this;

    if(instance === undefined){
      instance= self.get('session.data.authenticated.instance');
      this.set('instance',instance);
    }
    if(instance === undefined){
      if(this.get('session').get('isAuthenticated')){
        this.get('session').invalidate();
      }
      return;
    }

    this.get('session').authorize('authorizer:mastodon', (headerName, headerValue) => {
        let res= Ember.$.ajax({
          url: instance + '/api/v1/statuses',
          beforeSend: function(xhr) {
            xhr.setRequestHeader(headerName, headerValue);
          },
          method: 'POST',
          crossDomain: true,
          dataType: 'json',
          contentType: 'application/json; charset=utf-8',
          data: JSON.stringify(post_value)
        });
        res.then(function(resp) {
           let status = store.push('status',resp);
           status.save();
        }, function(reason) {
          // some error handling
          reason.then(function(error){
              console.log(error);
          });
        });
    });
  },
  query(method, url, data){
    console.log('api query');
    let instance = this.get('instance');
    let store   = this.get('store');
    let self = this;

    if(instance === undefined){
      instance= self.get('session.data.authenticated.instance');
      this.set('instance',instance);
    }
    if(instance === undefined){
      if(this.get('session').get('isAuthenticated')){
        this.get('session').invalidate();
      }
      return;
    }
      return new Ember.RSVP.Promise((resolve, reject) => {
        this.get('session').authorize('authorizer:mastodon', (headerName, headerValue) => {
            let res= Ember.$.ajax({
              url: instance + url,
              beforeSend: function(xhr) {
                xhr.setRequestHeader(headerName, headerValue);
              },
              method: method,
              crossDomain: true,
              dataType: 'json',
              contentType: 'application/json; charset=utf-8',
              data: JSON.stringify(data)
            }).then(function(response) {
                Ember.run(function() {
                    resolve(response);
                });
            }, function(xhr) {
              console.log('error');
                Ember.run(function() {
                    reject(response);
                });
           });
        });
      });
  },

  //DELETE /api/v1/statuses/:id
  delete(status){
      console.log('api delete');
      this.query("DELETE", "/api/v1/statuses/"+status.id,{});
  },

  //POST /api/v1/statuses/:id/favourite
  favourite(status){
      let store   = this.get('store');
      console.log('api favourite');
      let resp = this.query("POST", "/api/v1/statuses/"+status.id+"/favourite",{});
      status = store.push('status',resp);
      status.save();
  },

  //POST /api/v1/statuses/:id/unfavourite
  unfavourite(status){
      let store   = this.get('store');
      console.log('api unfavourite');
      let resp = this.query("POST", "/api/v1/statuses/"+status.id+"/unfavourite",{});
      status = store.push('status',resp);
      status.save();
  },

  //POST /api/v1/statuses/:id/action
  status_action(status,action){
      let store   = this.get('store');
      console.log('api : '+action);
      let res = this.query("POST", "/api/v1/statuses/"+status.id+"/"+action,{});
      console.log(res);
      res.then(function(resp) {
        console.log('query: ');
        console.log(status.id + ' : ' +resp.id);
        store.push(store.normalize('status',resp));
        //status = store.push('status',resp);
        //status.save();
      }, function(reason) {
        // some error handling
        reason.then(function(error){
            console.log(error);
        });
      });
  }


});
