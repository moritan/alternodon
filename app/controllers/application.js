import Ember from 'ember';
export default Ember.Controller.extend({
  session: Ember.inject.service('session'),
  store: Ember.inject.service('store'),
  actions: {
    transitionToLoginRoute() {
      this.transitionToRoute('login');
    },
    invalidateSession() {
      this.get('session').invalidate();
    }
  }
});
