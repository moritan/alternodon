// app/controllers/login.js
import Ember from 'ember';

export default Ember.Controller.extend({
  session: Ember.inject.service('session'),

  actions: {
    authenticate() {
      this.get('session').authenticate('authenticator:mastodon', this.getProperties('instance','identification', 'password')).catch((reason) => {
          console.log('error');
          console.log('reason.error : '+reason.error);
          this.set('errorMessage', reason.error);
      });
    }
  }
});
