// app/controllers/login.js
import Ember from 'ember';

export default Ember.Controller.extend({
  session: Ember.inject.service('session'),
  mastodonApi: Ember.inject.service('mastodonApi'),
  reverseModel: function(){
    return this.get('model').toArray().reverse();
  }.property('model.@each'),

  actions: {
    update() {
      console.log('refresh');
      this.get('mastodonApi').timelines_home();
    }
  }
});
