import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

// app/router.js
Router.map(function() {
  this.route('login');
  this.route('about');
  this.route('protected');
});

export default Router;
