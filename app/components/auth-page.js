import Ember from 'ember';

export default Ember.Component.extend({
  fixedTop: false,
   transparent: true,
   navbar_initialized: false,
   misc_navbar_menu_visible: 0,
   session: Ember.inject.service('session'),
   store: Ember.inject.service('store'),
   websockets: Ember.inject.service(),
   socketRef: null,

   activePanel: {code:'home',title:'Home',icon:'ti-home', componentName:'home-list'},

  titlePanel: Ember.computed('activePanel', function() {
    let active = this.get('activePanel').title;
    let res = active[0].toUpperCase() + active.slice(1);
    console.log(res);
    return res;
  }),

  menuItems: [
    {code:'home',title:'Home',icon:'ti-home', componentName:'home-list'},
    {code:'notifications',title:'Notifications',icon:'ti-pin-alt', componentName:''},
    {code:'favorites',title:'favorites',icon:'ti-star', componentName:''},
    {code:'timelineLocale',title:'local timeline',icon:'ti-comments', componentName:''},
    {code:'timelineFederate',title:'Federated timeline',icon:'ti-infinite', componentName:''},
    {code:'settings',title:'Settings',icon:'ti-settings', componentName:''}
  ],

  actions: {
    invalidateSession() {
      this.get('session').invalidate();
    },
    test(value) {
      this.set('activePanel',value);
    }
  },

  didInsertElement:function(){
    this._super(...arguments);
    Ember.run.scheduleOnce('afterRender', this, function() {
      this.initRightMenu();
    });

    /*
      2. The next step you need to do is to create your actual websocket. Calling socketFor
      will retrieve a cached websocket if one exists or in this case it
      will create a new one for us.
    */
    let endpoint = this.get('session.data.authenticated.instance').replace('http','ws')+'/api/v1/streaming/?stream=user&access_token='+this.get('session.data.authenticated.access_token');
    const socket = this.get('websockets').socketFor(endpoint);

    /*
      3. The next step is to define your event handlers. All event handlers
      are added via the `on` method and take 3 arguments: event name, callback
      function, and the context in which to invoke the callback. All 3 arguments
      are required.
    */
    socket.on('open', this.myOpenHandler, this);
    socket.on('message', this.myMessageHandler, this);
    socket.on('close', this.myCloseHandler, this);

    this.set('socketRef', socket);
  },

  willDestroyElement() {
    this._super(...arguments);
    const socket = this.get('socketRef');

    /*
      4. The final step is to remove all of the listeners you have setup.
    */
    socket.off('open', this.myOpenHandler);
    socket.off('message', this.myMessageHandler);
    socket.off('close', this.myCloseHandler);
  },

  myOpenHandler(event) {
    console.log(`On open event has been called`);
  },

  myMessageHandler(event) {
    let store = this.get('store');
    let data = JSON.parse(event.data);
    //console.log(data);
    if(data.event ==='update'){
      let item = JSON.parse(data.payload);
      let status = store.createRecord('status',item);
      status.save();
    }else if(data.event ==='delete'){
      store.find('status',data.payload).then(function(status){
          //console.log(status);
          status.deleteRecord();
          status.save();
      });
    }else   if(data.event ==='notification'){
        let item = JSON.parse(data.payload);
        let notif = store.createRecord('notification',item);
        notif.save();
      }else {
        console.log('unknown event');
      }
  },

  myCloseHandler(event) {
    console.log(`On close event has been called`);
  },

  checkScrollForTransparentNavbar: debounce(function() {
      if(Ember.$(document).scrollTop() > 381 ) {
          if(this.get('transparent')) {
              this.set('transparent',false);
              Ember.$('.navbar-color-on-scroll').removeClass('navbar-transparent');
              Ember.$('.navbar-title').removeClass('hidden');
          }
      } else {
          if( !this.get('transparent')) {
              this.set('transparent',true);
              Ember.$('.navbar-color-on-scroll').addClass('navbar-transparent');
              Ember.$('.navbar-title').addClass('hidden');
          }
      }
  }),
  initRightMenu: function(){
      let self = this;
       if(!self.get('navbar_initialized')){
          Ember.$off_canvas_sidebar = Ember.$('nav').find('.navbar-collapse').first().clone(true);

          Ember.$sidebar = Ember.$('.sidebar');
          let sidebar_bg_color = Ember.$sidebar.data('background-color');
          let sidebar_active_color = Ember.$sidebar.data('active-color');

          Ember.$logo = Ember.$sidebar.find('.logo').first();
          let logo_content = Ember.$logo[0].outerHTML;

          let ul_content = '';
          let content_buff='';

          // set the bg color and active color from the default sidebar to the off canvas sidebar;
          Ember.$off_canvas_sidebar.attr('data-background-color',sidebar_bg_color);
          Ember.$off_canvas_sidebar.attr('data-active-color',sidebar_active_color);

          Ember.$off_canvas_sidebar.addClass('off-canvas-sidebar');

          //add the content from the regular header to the right menu
          Ember.$off_canvas_sidebar.children('ul').each(function(){
             content_buff = Ember.$(this).html();
              ul_content = ul_content + content_buff;
          });

          // add the content from the sidebar to the right menu
          content_buff = Ember.$sidebar.find('.nav').html();
          ul_content = ul_content + '<li class="divider"></li>'+ content_buff;

          ul_content = '<ul class="nav navbar-nav">' + ul_content + '</ul>';

          let navbar_content = logo_content + ul_content;
          navbar_content = '<div class="sidebar-wrapper">' + navbar_content + '</div>';
          Ember.$off_canvas_sidebar.html(navbar_content);

          Ember.$('body').append(Ember.$off_canvas_sidebar);

           Ember.$toggle = Ember.$('.navbar-toggle');

           Ember.$off_canvas_sidebar.find('a').removeClass('btn btn-round btn-default');
           Ember.$off_canvas_sidebar.find('button').removeClass('btn-round btn-fill btn-info btn-primary btn-success btn-danger btn-warning btn-neutral');
           Ember.$off_canvas_sidebar.find('button').addClass('btn-simple btn-block');

           Ember.$toggle.click(function (){
              if(self.get('misc_navbar_menu_visible') == 1) {
                  Ember.$('html').removeClass('nav-open');
                  self.set('misc_navbar_menu_visible', 0);
                  Ember.$('#bodyClick').remove();
                   setTimeout(function(){
                      Ember.$toggle.removeClass('toggled');
                   }, 400);

              } else {
                  setTimeout(function(){
                      Ember.$toggle.addClass('toggled');
                  }, 430);

                  let div = '<div id="bodyClick"></div>';
                  Ember.$(div).appendTo("body").click(function() {
                      Ember.$('html').removeClass('nav-open');
                      self.set('misc_navbar_menu_visible', 0);
                      Ember.$('#bodyClick').remove();
                       setTimeout(function(){
                          Ember.$toggle.removeClass('toggled');
                       }, 400);
                  });

                  Ember.$('html').addClass('nav-open');
                  self.set('misc_navbar_menu_visible', 1);

              }
          });
          self.set('navbar_initialized',true);
      }
  }
});


// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.

function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		clearTimeout(timeout);
		timeout = setTimeout(function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		}, wait);
		if (immediate && !timeout) func.apply(context, args);
	};
}
