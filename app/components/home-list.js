import Ember from 'ember';

export default Ember.Component.extend({
  session: Ember.inject.service('session'),
  store: Ember.inject.service('store'),
  mastodonApi: Ember.inject.service('mastodonApi'),
  post_value:{
    status: ''
  },
  model: function() {
    return this.get('store').findAll('status');
  }.property(),
  reverseModel: function(){
    return this.get('model').sortBy("created_at").toArray().reverse();
  }.property('model.@each'),

  actions: {
    update() {
      this.get('mastodonApi').timelines_home();
      //this.get('model')
    },
    post() {
      //console.log(Ember.$('#myModal'));
      //console.log(this.get('post_value'));
      this.get('mastodonApi').post_status(this.get('post_value'));
      //this.send('modal.close');
    }
  }
});
