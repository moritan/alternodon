import Ember from 'ember';

export default Ember.Component.extend({
  mastodonApi: Ember.inject.service('mastodonApi'),
  showTools: false,
  mouseEnter() {
    this.set('showTools', true);
  },
  mouseLeave() {
    this.set('showTools', false);
  },
  actions: {
    favorite() {
      //this.get('mastodonApi').timelines_home();
      let status = this.get('status');
      let favourited = status.get('favourited');
      let action = (favourited!=null&&favourited)?'unfavourite':'favourite';
      console.log(action);
      this.get('mastodonApi').status_action(status,action);
    },
    boost() {
     let status = this.get('status');
    //console.log(status);
    let reblogged = status.get('reblogged');
      console.log(reblogged);
      let action = (reblogged!=null&&reblogged)?'unreblog':'reblog';
      //    _status = (reblogged!=null&&reblogged)?_status.get('reblog'):_status;
      //console.log(action);
      //console.log(status);
      status.set('reblogged',!reblogged);
      status.save();
      reblogged = status.get('reblogged');
      console.log(reblogged);

     this.get('mastodonApi').status_action(status,action);
    },
    delete() {
      this.get('mastodonApi').delete(this.get('status'));
    },
    reply() {
      //this.get('mastodonApi').timelines_home();
      console.log('reply');
    }
  }
});
