import Ember from 'ember';

export default Ember.Component.extend({
  tagName:'',
  mastodonApi: Ember.inject.service('mastodonApi'),
  post_value:{
    status: ''
  },
  actions: {
    post() {
      this.get('mastodonApi').post_status(this.get('post_value'));
    }
  }
});
